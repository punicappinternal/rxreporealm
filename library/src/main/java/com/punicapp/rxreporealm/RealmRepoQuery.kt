package com.punicapp.rxreporealm

import android.util.Log
import android.util.Pair
import com.punicapp.rxrepocore.AbstractQuery
import com.punicapp.rxrepocore.Check
import com.punicapp.rxrepocore.LocalFilter
import com.punicapp.rxrepocore.SortDir
import io.realm.*
import java.util.*

internal class RealmRepoQuery<out T : RealmObject>(private val clazz: Class<T>) : AbstractQuery<T>() {
    private lateinit var realmInstance: Realm

    private val sortParameters: Pair<Array<String>, Array<Sort>>?
        get() {
            return sortParamesGetterCalc()
        }

    private fun sortParamesGetterCalc(): Pair<Array<String>, Array<Sort>>? {
        if (sorting.size == 0) {
            return null
        }
        val property = ArrayList<String>()
        val sorts = ArrayList<Sort>()

        sorting.forEach {
            property.add(it.property)
            sorts.add(if (it.sort == SortDir.Asc) Sort.ASCENDING else Sort.DESCENDING)
        }
        val fieldNames = property.toTypedArray()
        val sortOrders = sorts.toTypedArray()
        return Pair(fieldNames, sortOrders)
    }

    fun setRealmInstance(realmInstance: Realm) {
        this.realmInstance = realmInstance
    }

    override fun find(): List<T> {
        val sortParameters = sortParameters
        val realmQuery = buildRealmQuery(realmInstance)
        val results: RealmResults<T>
        results = if (sortParameters != null)
            realmQuery.findAllSorted(sortParameters.first, sortParameters.second)
        else
            realmQuery.findAll()
        return realmInstance.copyFromRealm(results)
    }

    override fun first(): T? {
        val sortParameters = sortParameters

        val query = buildRealmQuery(realmInstance)
        val first = if (sortParameters != null)
            query.findAllSorted(sortParameters.first, sortParameters.second)[0]
        else {
            query.findFirst()
        }

        return first?.let { realmInstance.copyFromRealm(it) }
    }

    override fun count(): Long {
        return buildRealmQuery(realmInstance).count()
    }

    override fun remove(): Int {
        realmInstance.beginTransaction()
        val all = buildRealmQuery(realmInstance).findAll()
        val size = all.size
        val success = all.deleteAllFromRealm()
        realmInstance.commitTransaction()
        return if (success) size else 0
    }

    private fun buildRealmQuery(instance: Realm?): RealmQuery<T> {
        var query = instance!!.where(clazz)

        filters.forEach {
            query = applyFilter(query, it)
        }

        return query
    }

    private fun applyFilter(query: RealmQuery<T>, filter: LocalFilter): RealmQuery<T> {
        val check = filter.check
        val prop = filter.idProp
        val value = filter.value
        when (check) {
            Check.Equal -> handleEq(query, prop, value)
            Check.EqualIgnoreCase -> handleEqIgnoreCase(query, prop, value)
            Check.NotEqual -> handleNeq(query, prop, value)
            Check.Great -> handleGor(query, prop, value)
            Check.GreatOrEqual -> handleGorEq(query, prop, value)
            Check.Less -> handleLor(query, prop, value)
            Check.LessOrEqual -> handleLorEq(query, prop, value)
            Check.IsNull -> query.isNull(prop)
            Check.IsNotNull -> query.isNotNull(prop)
            Check.In -> handleIn(query, prop, value)
            Check.Contains, Check.ContainsIgnoreCase -> {
                handleContains(query, prop, value, check)
            }
            Check.Between -> handleBetween(query, prop, value)
            Check.Like -> handleLike(query, prop, value)
        }
        return query
    }

    private fun warningWrongType(check: Check, value: Any) {
        Log.w("RxRepoRealm", "Can't handle $check: $value is of wrong type for this check")
    }

    private fun handleGorEq(query: RealmQuery<T>, prop: String, value: Any) {
        when (value) {
            is Int -> query.greaterThanOrEqualTo(prop, value)
            is Long -> query.greaterThanOrEqualTo(prop, value)
            is Double -> query.greaterThanOrEqualTo(prop, value)
            is Float -> query.greaterThanOrEqualTo(prop, value)
            is Date -> query.greaterThanOrEqualTo(prop, value)
            else -> warningWrongType(Check.GreatOrEqual, value)
        }
    }

    private fun handleGor(query: RealmQuery<T>, prop: String, value: Any) {
        when (value) {
            is Int -> query.greaterThan(prop, value)
            is Long -> query.greaterThan(prop, value)
            is Double -> query.greaterThan(prop, value)
            is Float -> query.greaterThan(prop, value)
            is Date -> query.greaterThan(prop, value)
            else -> warningWrongType(Check.Great, value)
        }
    }

    private fun handleLorEq(query: RealmQuery<T>, prop: String, value: Any) {
        when (value) {
            is Int -> query.lessThanOrEqualTo(prop, value)
            is Long -> query.lessThanOrEqualTo(prop, value)
            is Double -> query.lessThanOrEqualTo(prop, value)
            is Float -> query.lessThanOrEqualTo(prop, value)
            is Date -> query.lessThanOrEqualTo(prop, value)
            else -> warningWrongType(Check.LessOrEqual, value)
        }
    }

    private fun handleLor(query: RealmQuery<T>, prop: String, value: Any) {
        when (value) {
            is Int -> query.lessThan(prop, value)
            is Long -> query.lessThan(prop, value)
            is Double -> query.lessThan(prop, value)
            is Float -> query.lessThan(prop, value)
            is Date -> query.lessThan(prop, value)
            else -> warningWrongType(Check.Less, value)
        }
    }

    private fun handleBetween(query: RealmQuery<T>, prop: String, value: Any) {
        if (value is Array<*>)
            when (value[0]) {
                is Int -> query.between(prop, value[0] as Int, value[1] as Int)
                is Long -> query.between(prop, value[0] as Long, value[1] as Long)
                is Double -> query.between(prop, value[0] as Double, value[1] as Double)
                is Float -> query.between(prop, value[0] as Float, value[1] as Float)
                is Date -> query.between(prop, value[0] as Date, value[1] as Date)
                else -> warningWrongType(Check.Between, value)
            }
        else warningWrongType(Check.Between, value)

    }

    private fun handleLike(query: RealmQuery<T>, prop: String, value: Any) {
        if (value is String) {
            query.like(prop, value)
        } else {
            warningWrongType(Check.Like, value)
        }

    }

    private fun handleIn(query: RealmQuery<T>, prop: String, value: Any) {
        if (value is Array<*>)
            when (value[0]) {
                is String -> query.`in`(prop, value as Array<String>)
                is Byte -> query.`in`(prop, value as Array<Byte>)
                is Short -> query.`in`(prop, value as Array<Short>)
                is Int -> query.`in`(prop, value as Array<Int>)
                is Long -> query.`in`(prop, value as Array<Long>)
                is Double -> query.`in`(prop, value as Array<Double>)
                is Float -> query.`in`(prop, value as Array<Float>)
                is Boolean -> query.`in`(prop, value as Array<Boolean>)
                is Date -> query.`in`(prop, value as Array<Date>)
                else -> warningWrongType(Check.In, value)
            }
        else warningWrongType(Check.In, value)
    }

    private fun handleNeq(query: RealmQuery<T>, prop: String, value: Any) {
        when (value) {
            is String -> query.notEqualTo(prop, value)
            is Byte -> query.notEqualTo(prop, value)
            is ByteArray -> query.notEqualTo(prop, value)
            is Short -> query.notEqualTo(prop, value)
            is Int -> query.notEqualTo(prop, value)
            is Long -> query.notEqualTo(prop, value)
            is Double -> query.notEqualTo(prop, value)
            is Float -> query.notEqualTo(prop, value)
            is Boolean -> query.notEqualTo(prop, value)
            is Date -> query.notEqualTo(prop, value)
            else -> warningWrongType(Check.NotEqual, value)
        }
    }

    private fun handleEq(query: RealmQuery<T>, prop: String, value: Any) {
        when (value) {
            is String -> query.equalTo(prop, value)
            is Byte -> query.equalTo(prop, value)
            is ByteArray -> query.equalTo(prop, value)
            is Short -> query.equalTo(prop, value)
            is Int -> query.equalTo(prop, value)
            is Long -> query.equalTo(prop, value)
            is Double -> query.equalTo(prop, value)
            is Float -> query.equalTo(prop, value)
            is Boolean -> query.equalTo(prop, value)
            is Date -> query.equalTo(prop, value)
            else -> warningWrongType(Check.Equal, value)
        }
    }

    private fun handleEqIgnoreCase(query: RealmQuery<T>, prop: String, value: Any) {
        if (value is String) {
            query.equalTo(prop, value, Case.INSENSITIVE)
        } else {
            warningWrongType(Check.EqualIgnoreCase, value)
        }
    }

    private fun handleContains(query: RealmQuery<T>, prop: String, value: Any, check: Check) {
        if (value is String) {
            query.contains(prop, value, if (check == Check.Contains) Case.SENSITIVE else Case.INSENSITIVE)
        } else {
            warningWrongType(check, value)
        }
    }
}
